import BaseController from "./BaseController";
import EmailModel from "@root/app/Models/EmailModel";
import ApiException from "@root/app/Exceptions/ApiException";
import _ from "lodash";
class EmailController extends BaseController {
  Model = EmailModel;

  /********** CRUD **********/
  async index() {
    let inputs = this.request.all();
    inputs = this.defaultSorting(inputs, "createdAt", "desc");
    const result = await this.Model.query()
      .select(this.Model.allowSelect)
      .getForGridTable(inputs);
    return result;
  }

  async detail() {
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
    };
    const { id } = this.validate(inputs, allowFields);
    const record = await this.Model.query().findById(id);
    if (!record) throw new ApiException(7104, "Data does not exist.");
    return record;
  }

  async store(): Promise<any> {
    const { auth } = this.request;
    const allowFields = {
      email: "string!",
      type: "string!",
      smtp: {
        host: "string",
        port: "string",
        password: "string",
      },
      sendGrid: {
        secret: "string"
      },
      ses: {
        secret: "string"
      }
    };
    const inputs = this.request.all();

    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    
    const email = params.email.toLowerCase().trim();
    const checkEmail = await this.Model.query().findOne({ email: email });
    if (checkEmail) throw new ApiException(7103, "Email already exists.");

    const {smtp = {}, sendGrid= {}, ses= {}}  = params;
    delete params.smtp;
    delete params.sendGrid;
    delete params.ses;
    params.information = { smtp,  sendGrid, ses }

    params.userId = auth.id
    return await this.Model.query().insert(params);
  }

  async update(): Promise<any> {
    const { auth } = this.request;
    const allowFields = {
      id: "number!",
      email: "string!",
      type: "string!",
      smtp: {
        host: "string",
        port: "number",
        password: "string",
      },
      sendGrid: {
        secret: "string"
      },
      ses: {
        secret: "string"
      }
    };

    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const { id } = params;
    delete params.id;

    const record = await this.Model.query().findById(id);
    if (!record) throw new ApiException(7104, "Data doesn't exist!");

    const email = params.email.trim();
    const checkEmail = await this.Model.query()
      .where("email", email)
      .whereNot("id", id)
      .first();
    if (checkEmail) {
      throw new ApiException(7103, "Email already exists!");
    }

    const {smtp = {}, sendGrid= {}, ses= {}}  = params;
    delete params.smtp;
    delete params.sendGrid;
    delete params.ses;
    params.information = { smtp,  sendGrid, ses }

    const result = await this.Model.query().patchAndFetchById(id, params);
    return {
      result,
      origin: record,
    };
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const record = await this.Model.query().findById(params.id);
    if (!record) return { message: "" };

    await record.$query().del();
    return { message: `Delete successfully`, origin: record };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields);
    const records = await this.Model.query().whereIn("id", params.ids);
    if (records.length === 0) return { message: "" };

    const result = await this.Model.query().whereIn("id", params.ids).del();
    return { message: `Delete ${result} records successfully`, origin: records };
  }
  /**************************************** END ****************************************/

}

export default EmailController;
