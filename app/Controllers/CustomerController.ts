import BaseController from "./BaseController";
import CustomerModel from "@root/app/Models/CustomerModel";
import ApiException from "@root/app/Exceptions/ApiException";
import _ from "lodash";
class CustomerController extends BaseController {
  Model = CustomerModel;

  /********** CRUD **********/
  async index() {
    let inputs = this.request.all();
    inputs = this.defaultSorting(inputs, "createdAt", "desc");
    const result = await this.Model.query()
      .select(this.Model.allowSelect)
      .getForGridTable(inputs);
    return result;
  }

  async detail() {
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
    };
    const { id } = this.validate(inputs, allowFields);
    const record = await this.Model.query().findById(id);
    if (!record) throw new ApiException(7104, "Data does not exist.");
    return record;
  }

  async store() {
    const { auth } = this.request;
    const allowFields = {
      email: "string!",
      firstName: "string",
      lastName: "string",
      address: "string",
      phone: "string",
    };
    const inputs = this.request.all();

    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const email = params.email.toLowerCase().trim();
    const checkEmail = await this.Model.query().findOne({ email: email });
    if (checkEmail) throw new ApiException(7103, "Customer already exists.");

    params.userId = auth.id
    return await this.Model.query().insert(params);
  }

  async storeMany() {
    const { auth } = this.request;
    const inputs = this.request.all();

    const allowFields = {
      data: [
        {
          email: "string!",
          firstName: "string",
          lastName: "string",
          address: "string",
          phone: "string",
        },
      ],
    };
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });

    let count = 0;
    for (const row of params.data) {
      const email = row.email.toLowerCase().trim();
      const checkExist = await this.Model.query().findOne({ email });
      if (!checkExist) {
        row.userId = auth.id
        await this.Model.query().insert(row);
        count++;
      }
    }

    return { message: `Added success ${count} PTT IDs` };
  }

  async update() {
    const { auth } = this.request;
    const allowFields = {
      id: "number!",
      email: "string!",
      firstName: "string",
      lastName: "string",
      address: "string",
      phone: "string",
    };

    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const { id } = params;
    delete params.id;

    const record = await this.Model.query().findById(id);
    if (!record) throw new ApiException(7104, "Data doesn't exist!");

    const email = params.email.toLowerCase().trim();
    const checkEmail = await this.Model.query()
      .where("email", email)
      .whereNot("id", id)
      .first();
    if (checkEmail) {
      throw new ApiException(7103, "Email already exists!");
    }

    const result = await this.Model.query().patchAndFetchById(id, params);
    return {
      result,
      origin: record,
    };
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const record = await this.Model.query().findById(params.id);
    if (!record) return { message: "" };

    await record.$query().del();
    return { message: `Delete successfully`, origin: record };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields);
    const records = await this.Model.query().whereIn("id", params.ids);
    if (records.length === 0) return { message: "" };

    const result = await this.Model.query().whereIn("id", params.ids).del();
    return {
      message: `Delete ${result} records successfully`,
      origin: records,
    };
  }
  /**************************************** END ****************************************/
}

export default CustomerController;
