import BaseController from "./BaseController";
import TemplateModel from "@root/app/Models/TemplateModel";
import ApiException from "@root/app/Exceptions/ApiException";
import _ from "lodash";
import FileUtil from "@app/Services/Utils/File";
import { v4 as uuidv4 } from "uuid";
import Env from "@core/Env";

const TEMPLATE_FOLDER = Env.get("TEMPLATE_FOLDER", "templates");
class TemplateController extends BaseController {
  Model = TemplateModel;

  /********** CRUD **********/
  async index() {
    let inputs = this.request.all();
    inputs = this.defaultSorting(inputs, "createdAt", "desc");
    const result = await this.Model.query()
      .select(this.Model.allowSelect)
      .getForGridTable(inputs);
    return result;
  }

  async detail() {
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
    };
    const { id } = this.validate(inputs, allowFields);
    const record = await this.Model.query().findById(id);
    if (!record) throw new ApiException(7104, "Data does not exist.");
    return record;
  }

  async store() {
    const { auth } = this.request;
    const allowFields = {
      name: "string!",
    };
    const inputs = this.request.all();
    const files = this.request.files;

    if (!files || files.length === 0 || files.length > 1) throw new ApiException(7115, "Required 1 file to create template");

    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const name = params.name.trim();
    const checkName = await this.Model.query().findOne({ name });
    if (checkName) throw new ApiException(7103, "Template name already exists.");

    const data = await this._saveTemplate(files[0]);

    params.sourcePath = data.path;
    params.fileName = data.fileName;
    params.fileSize = data.fileSize;
    params.filePath = data.filePath;
    params.md5 = data.md5;

    const checkMd5 = await this.Model.query().findOne({md5: params.md5});
    if (checkMd5) {
      const { fileName, filePath, sourcePath } = params
      await this._removeTemplate({fileName, filePath, sourcePath}) // remove files and folders here
      throw new ApiException(500, "Template file already exists.")
    }
    params.userId = auth.id

    return await this.Model.query().insert(params);
  }

  async update() {
    const { auth } = this.request;
    const allowFields = {
      id: "number!",
      name: "string!"
    };

    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const { id } = params;
    delete params.id;

    const record = await this.Model.query().findById(id);
    if (!record) throw new ApiException(7104, "Data doesn't exist!");

    const name = params.name.trim();
    const checkName = await this.Model.query()
      .where("name", name)
      .whereNot("id", id)
      .first();
    if (checkName) {
      throw new ApiException(7103, "Email already exists!");
    }

    const result = await this.Model.query().patchAndFetchById(id, params);
    return {
      result,
      origin: record,
    };
  }

  async destroy() {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const record = await this.Model.query().findById(params.id);
    if (!record) throw new ApiException(7114, "Data doesn't exist!");
    await record.$query().del();
    await this._removeTemplate(record);
    return { message: `Delete successfully`, origin: record };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields);
    const records = await this.Model.query().whereIn("id", params.ids);
    if (records.length !== params.ids.length)
      throw new ApiException(7114, "Data doesn't exist!");

    try {
      const transaction = await this.Model.transaction(async (trx) => {
        for (const record of records) {
          await record.$query(trx).del();
          await this._removeTemplate(record);
        }
      });
    } catch (e) {
      throw new Error(e);
    }

    return {
      message: `Delete ${records.length} records successfully`,
      origin: records,
    };
  }
  /****************************** END ******************************/

  /********** ULTIS **********/
  async _saveTemplate(file: any) {
    const fileExt: string = FileUtil.getFileExtension(file.originalname);
    const fileName: string = uuidv4() + fileExt;
    const fileSize: number = file.size;
    const directory = `${TEMPLATE_FOLDER}`;
    const result = await FileUtil.uploadFile({
        directory,
        data: file,
        fileName: fileName,
        overwrite: true,
      });
    const templatePath = result.filePath + "/" + fileName.replace(fileExt, "")
    const extract = await FileUtil.unzipFile(result.filePath + "/" + fileName, templatePath)
    return {
      fileName,
      fileSize,
      filePath: result.filePath,
      path: templatePath,
      md5: result.md5,
    };
  }

  async _removeTemplate(template) {
    try {
      await FileUtil.removeFile(template.filePath + "/" + template.fileName)
      await FileUtil.removeFolder(template.sourcePath)
    } catch (error) {
      throw new ApiException(500, "_removeTemplate.error" ,error);
    }
    return true;
  }

  /****************************** END ******************************/


}

export default TemplateController;
