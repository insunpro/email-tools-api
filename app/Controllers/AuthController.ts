import BaseController from "./BaseController";
import UserModel from "@root/app/Models/UserModel";
import RoleModel from "@root/app/Models/RoleModel";
import ApiException from "@root/app/Exceptions/ApiException";
import TemplateEmail from "@config/email_templates";
import Auth from "@root/libs/Auth";
import authConfig from "@config/auth";
import to from "await-to-js";
import Env from "@core/Env";

// import OTPService from '@root/app/Services/OTP'

const ADMIN_URL = Env.get("ADMIN_URL", "http://localhost");

class AuthController extends BaseController {
  Model = UserModel;
  roleModel = RoleModel;

  /********** EXTENDS **********/
  async login() {
    const inputs = this.request.all();
    const allowFields = {
      username: "string!",
      password: "string!",
    };

    const data = this.validate(inputs, allowFields, { removeNotAllow: true });
    const user = await this.Model.checkLogin({
      username: data.username,
      password: data.password,
    });
    if (!user) throw new ApiException(7000, "Can not login");
    const permissions = {};
    const token = Auth.generateJWT(
      {
        id: user.id,
        username: user.username,
        permissions: permissions,
        role: user.role
      },
      {
        key: authConfig["SECRET_KEY_ADMIN"],
        expiresIn: authConfig["JWT_EXPIRE_ADMIN"],
      }
    );
    delete data.password;

    this.response.success({
      token,
      user: {
        ...user,
        // type: user.groupId === 1 ? type : "user",
        permissions,
      },
    });
  }

  async forgotPassword() {
    const allowFields = {
      email: "string!",
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const exist = await this.Model.query().findOne({ email: params.email });
    if (!exist) throw new ApiException(7010, "Email does't exist!");
    const { subject, content } = TemplateEmail["forgotPassword"];
    //sent email
    const variables = {
      resetPasswordLink: this.makeForgotPasswordLink(exist),
      username: exist.username,
      email: exist.email,
    };
    console.log(variables);
    // throw new ApiException(7001, "User does't exist!");

    delete exist.password
    return exist;
  }

  async resetPassword() {
    const allowFields = {
      token: "string!",
      newPassword: "string!",
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const [error, auth] = await to(
      Auth.verify(params.token, {
        key: authConfig["SECRET_KEY_ADMIN"],
      })
    );
    if (error) throw new ApiException(7002, "The token has expired");
    const user = await this.Model.query().findById(auth.id);
    if (!user) throw new ApiException(7001, "User doesn't exist!");
    const hash = await this.Model.hash(params.newPassword);
    return await this.Model.query().patchAndFetchById(user.id, {
      password: hash,
    });
  }

  async checkToken() {
    const allowFields = {
      token: "string!",
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const [error, auth] = await to(
      Auth.verify(params.token, {
        key: authConfig["SECRET_KEY_ADMIN"],
      })
    );
    if (error) throw new ApiException(7002, "The token has expired");
    const user = await this.Model.query().findById(auth.id);
    if (!user) throw new ApiException(7001, "User doesn't exist!");
    delete user.password;
    return user;
  }
  /********** END **********/

  /********** UTILS **********/
  makeForgotPasswordLink(user: any) {
    const token = Auth.generateJWT(
      {
        id: user.id,
        username: user.username,
        name: user.name,
        email: user.email,
      },
      {
        key: authConfig["SECRET_KEY_ADMIN"],
        expiresIn: authConfig["JWT_EXPIRE_VERYFY_EMAIL"],
      }
    );
    return `${ADMIN_URL}/reset/${token}`;
  }
  /********** END **********/
}

export default AuthController;
