import BaseController from "./BaseController";
import CampaignModel from "@root/app/Models/CampaignModel";
import ApiException from "@root/app/Exceptions/ApiException";
import _ from "lodash";
import TemplateModel from "../Models/TemplateModel";
import CustomerModel from "../Models/CustomerModel";
import moment from "moment"
class CampaignController extends BaseController {
  Model = CampaignModel;
  templateModel = TemplateModel;
  customerModel = CustomerModel;


  /********** CRUD **********/
  async index(): Promise<any> {
    let inputs = this.request.all();
    inputs = this.defaultSorting(inputs, "createdAt", "desc");
    const result = await this.Model.query()
      .select(this.Model.allowSelect)
      .getForGridTable(inputs);
    return result;
  }

  async detail(): Promise<any> {
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
    };
    const { id } = this.validate(inputs, allowFields);
    const record = await this.Model.query().findById(id);
    if (!record) throw new ApiException(7104, "Data does not exist.");
    return record;
  }

  async store(): Promise<any> {
    const { auth } = this.request;
    const allowFields = {
      name: "string!",
      emailId: "number!",
      templateId: "number!",
      startTime: "string!",
      delayTime: "number!",
      customerIds: ["number!"]
    };
    const inputs = this.request.all();

    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const name = params.name.trim();
    const checkName = await this.Model.query().findOne({ name: name });
    if (checkName) throw new ApiException(7103, "Campaign already exists.");

    const checkTemplate = await this.templateModel.query().findById(params.templateId);
    if (!checkTemplate) throw new ApiException(7103, "Template does not exists.");

    const { customerIds } = params;
    const customers = await this.customerModel.query().whereIn("id", customerIds);
    if (customers.length != customerIds.length) throw new ApiException(500, "Customer does not exists")

    delete params.customerIds;
    params.startTime = moment(params.startTime, "YYYY-MM-DD HH:mm:ss", true);
    if (!params.startTime.isValid()) throw new ApiException(400, "Start time is not valid")

    params.jobs = customerIds.map(customerId => {
      return {
        customerId
      }
    })

    params.userId = auth.id
    return await this.Model.query().insertGraph(params);
  }

  async update(): Promise<any> {
    const { auth } = this.request;
    const allowFields = {
      id: "number!",
      name: "string!",
      templateId: "string!",
      startTime: "string!",
    };

    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const { id } = params;
    delete params.id;

    const record = await this.Model.query().findById(id);
    if (!record) throw new ApiException(7104, "Data doesn't exist!");

    const name = params.name.toLowerCase().trim();
    if (name != record.name) {
      const checkname = await this.Model.query().findOne({name});
      if (checkname) {
        throw new ApiException(7103, "Campaign name already exists!");
      }
    }

    const checkTemplate = await this.templateModel.query().findById(params.templateId);
    if (!checkTemplate) throw new ApiException(7103, "Template does not exists.");

    params.startTime = moment(params.startTime, "YYYY-MM-DD HH:mm:ss", true);
    if (!params.startTime.isValid()) throw new ApiException(400, "Start time is not valid")

    const result = await this.Model.query().patchAndFetchById(id, params);
    return {
      result,
      origin: record,
    };
  }

  async destroy(): Promise<any> {
    const allowFields = {
      id: "number!",
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const record = await this.Model.query().findById(params.id);
    if (!record) return { message: "" };

    await record.$query().del();
    return { message: `Delete successfully`, origin: record };
  }

  async delete() {
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields);
    const records = await this.Model.query().whereIn("id", params.ids);
    if (records.length === 0) return { message: "" };

    const result = await this.Model.query().whereIn("id", params.ids).del();
    return { message: `Delete ${result} records successfully`, origin: records };
  }
  /**************************************** END ****************************************/


  /**************************************** OTHERS ****************************************/
  async getCampaigns() {
    const campaigns = await this.Model.query();
    return campaigns
  }

  async getJobs(campaignId) {
    const campaign = await this.Model.query().findById(campaignId).withGraphFetched('jobs');
    return campaign
  }

  /**************************************** END ****************************************/
}

export default CampaignController;
