import BaseController from "./BaseController";
import UserModel from "@root/app/Models/UserModel";
import RoleModel from "@root/app/Models/RoleModel";
import ApiException from "@root/app/Exceptions/ApiException";
import Auth from "@root/libs/Auth";
import authConfig from "@config/auth";
// import OTPService from '@root/app/Services/OTP'

export default class AdminController extends BaseController {
  Model = UserModel;
  roleModel = RoleModel;

  /********** CRUD **********/
  async index() {
    const { auth } = this.request;
    let inputs = this.request.all();
    inputs = this.defaultSorting(inputs, "createdAt", "desc");
    const data = await this.Model.query()
      .select(this.Model.allowSelect)
      .withGraphJoined("role")
      .getForGridTable(inputs);

    return data;
  }

  async detail() {
    const { auth } = this.request;
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
    };
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });

    const data = await this.Model.query()
      .select(this.Model.allowSelect)
      .withGraphJoined("role")
      .findById(params.id);
    return data;
  }

  async store() {
    const { auth } = this.request;
    const inputs = this.request.all();
    const allowFields = {
      firstName: "string!",
      lastName: "string!",
      username: "string!",
      password: "string!",
      email: "string!",
      roleId: "number!",
      phone: "string",
    };
    let params = this.validate(inputs, allowFields, { removeNotAllow: true });

    const username = params.username.replace(
      /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
      ""
    );
    const email = params.email.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");

    const usernameExist = await this.Model.query().findOne({
      username: username,
    });
    if (usernameExist) throw new ApiException(7003, "Username already exists.");

    const emailExist = await this.Model.query().findOne({ email: email });
    if (emailExist) throw new ApiException(7004, "Email already exists.");

    const userRole = await this.roleModel.query().findById(params.roleId);
    if (!userRole) throw new ApiException(7005, "User role does not exist.");

    if (params["password"]) {
      params["password"] = await this.Model.hash(params["password"]);
    }

    params = {
      ...params,
      roleId: userRole.id,
      createdBy: auth.id,
    };
    const result = await this.Model.query().insert(params);
    delete result["password"];
    return result;
  }

  async update() {
    const inputs = this.request.all();
    const auth = this.request.auth;
    const allowFields = {
      id: "number!",
      firstName: "string!",
      lastName: "string!",
      email: "string!",
      phone: "string",
      roleId: "number",
      status: "number",
    };
    const params = this.validate(inputs, allowFields, { removeNotAllow: true });
    const { id } = params;
    delete params.id;
    const user = await this.Model.query()
      .select(this.Model.allowSelect)
      .findById(id);
    if (!user) throw new ApiException(7001, "User doesn't exists!");

    const email = params.email.trim();
    const checkEmail = await this.Model.query()
      .where("email", email.toLowerCase())
      .whereNot("id", user.id)
      .first();
    if (checkEmail) throw new ApiException(7004, "Email already exists!");

    params.updatedAt = new Date();
    params.updatedBy = auth.id;
    const result = await this.Model.query().findById(id).patch(params);
    delete user["password"];
    return { result, origin: user };
  }

  async delete() {
    const { auth } = this.request;
    const allowFields = {
      ids: ["number!"],
    };
    const inputs = this.request.all();
    const params = this.validate(inputs, allowFields);
    if (params.ids.includes(auth.id))
      throw new ApiException(7006, "You can not remove your account.");
    const users = await this.Model.query()
      .select(this.Model.allowSelect)
      .whereIn("id", params.ids);
    if (params.ids.length != users.length)
      throw new ApiException(7001, "User doesn't exists!");
    await this.Model.query().whereIn("id", params.ids).delete();
    return { message: `Delete successfully`, origin: users };
  }

  async destroy() {
    const { auth } = this.request;
    const params = this.request.all();
    const id = params.id;
    if (!id) throw new ApiException(9996, "ID is required!");
    if ([id].includes(auth.id))
      throw new ApiException(7006, "You can not remove your account.");
    const user = await this.Model.query()
      .select(this.Model.allowSelect)
      .where("id", params.id)
      .first();
    if (!user) throw new ApiException(7001, "User doesn't exists!");
    await this.Model.query().where("id", params.id).delete();
    return { message: `Delete successfully`, origin: user };
  }
  /********** END **********/

  /********** EXTENDS **********/
  async changePassword() {
    const inputs = this.request.all();
    const allowFields = {
      id: "number!",
      password: "string!",
    };
    const data = this.validate(inputs, allowFields, { removeNotAllow: true });
    const user = await this.Model.query().findById(data.id);
    if (!user) throw new ApiException(7001, "User doesn't exist!");
    const result = await user.changePassword(data["password"]);
    delete result["password"];
    return result;
  }

  /********** END **********/

  /********** UTILS **********/

  /********** END **********/
}
