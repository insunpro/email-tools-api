import Redis from 'ioredis'

const Pub = new Redis({
  port: Number(process.env.REDIS_PORT) || 6379, // Redis port
  host: process.env.REDIS_HOST || "127.0.0.1", // Redis host
  password: process.env.REDIS_PASS,
  db: Number(process.env.REDIS_DB) || 0,
});

const Sub = new Redis({
  port: Number(process.env.REDIS_PORT) || 6379, // Redis port
  host: process.env.REDIS_HOST || "127.0.0.1", // Redis host
  password: process.env.REDIS_PASS,
  db: Number(process.env.REDIS_DB) || 0,
});


export {
  Pub,
  Sub
}
