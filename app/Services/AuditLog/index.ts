import _debug from "debug";
const debug = _debug("AuditLog");
import UserLogModel from "@app/Models/UserLogModel";

const IGNORE_ACTION = ["index", "detail"];
const LARGE_ACTION = [];
const PARAMS_REMOVE = ["password", "otp", "secret", "confirmPassword"];
const IGNORE_CONTROLLER = ["RemoteController"];

class AuditLog {
  static async sendUserLog(
    auth,
    method: string,
    audit,
    request,
    isError = false,
    data: any = {}
  ): Promise<any> {
    if (isError) {
      //handle error
    }

    if (method.toLowerCase() == "get") {
      //handle get
      return;
    }

    const { id } = auth;
    const { controller, action } = audit;
    const { origin } = data;
    if (IGNORE_ACTION.includes(action)) {
      debug(`${controller}.${action} in ignore list.`);
      return;
    }
    if (IGNORE_CONTROLLER.includes(controller)) {
      debug(`${controller} in ignore list.`);
      return;
    }
    if (!id) {
      debug(`no userId`);
      return;
    }

    const requestData = { ...this.filterData(controller, action, request) };
    try {
      const createLog = await UserLogModel.query().insert({
        userId: id,
        controller,
        action,
        data: JSON.stringify(requestData),
        before: origin ? JSON.stringify(origin) : undefined,
      });
    } catch (err) {
      if (err) console.log(err);
    }
  }

  static filterData(controller: string, action: string, data: any): any {
    const route = `${controller}.${action}`;
    //filter param not allowed
    for (const param of PARAMS_REMOVE) {
      delete data[param];
    }

    //filter large data
    if (!LARGE_ACTION.includes(route)) return data;

    data["data"] = data["data"].length;
    return data;
  }
}

export default AuditLog;
