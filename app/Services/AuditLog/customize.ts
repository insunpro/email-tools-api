import _debug from "debug";
const debug = _debug("@ngochipx:dnc");
import UserLogModel from "@app/Models/UserLogModel";
import PttidLogModel from "@app/Models/PttidLogModel";
import { to } from "await-to-js";
import { create } from "lodash";

const IGNORE_ACTION = ["index", "detail"];
const LARGE_ACTION = [];
const PARAMS_REMOVE = ["password", "otp", "secret"];
const IGNORE_CONTROLLER = ["RemoteController"];

interface UserLog {
  userId: number;
  controller: string;
  action: string;
  method: string;
  data: any;
  before: any;
}
class CustomizeAuditLog {
  static async sendUserLog(log: UserLog) {
    const { userId, controller, action, method, data, before } = log;
    // console.log('before log', log)
    if (method.toLowerCase() == "get") {
      return;
    }
    if (IGNORE_ACTION.includes(action)) {
      debug(`${controller}.${action} in ignore list.`);
      return;
    }
    if (IGNORE_CONTROLLER.includes(controller)) {
      debug(`${controller} in ignore list.`);
      return;
    }
    if (!userId) {
      debug(`no userId`);
      return;
    }
    const description = { ...this.filterData(controller, action, data) };
    try {
      const createLog = await UserLogModel.query().insert({
        userId: userId,
        controller,
        action,
        data: JSON.stringify(description),
      });
      // console.log('after log', createLog)
    } catch (err) {
      if (err) console.log(err);
    }
  }

  static filterData(controller: string, action: string, data: any) {
    const route = `${controller}.${action}`;
    //filter param not allowed
    for (const param of PARAMS_REMOVE) {
      delete data[param];
    }

    //filter large data
    if (!LARGE_ACTION.includes(route)) return data;

    data["data"] = data["data"].length;
    return data;
  }
}

export default CustomizeAuditLog;
