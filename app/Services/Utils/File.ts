import fs from "fs-extra";
import path from "path";
import crypto from "crypto";
import extract from "extract-zip";

interface FileData {
  directory: string;
  data: any;
  fileName: string;
  overwrite: boolean;
}

class FileUtil {

  static uploadFile = async ({
    fileName,
    directory,
    data,
    overwrite = true,
  }: FileData): Promise<any> => {
    if (!data || !data.buffer) {
      throw new Error("invalid format: data");
    }
    directory = path.resolve(directory);
    if (!fileName) fileName = data.originalname;
    const pathFile = path.join(directory, fileName);

    if (!overwrite && fs.existsSync(pathFile)) {
      throw new Error("File exist!");
    }

    fs.ensureDirSync(directory);
    fs.writeFileSync(pathFile, data.buffer);

    return {
      filePath: directory,
      md5: crypto.createHash("md5").update(data.buffer, "utf8").digest("hex")
    }
  };

  static removeFile = async (filePath: string): Promise<any> => {
    const pathToFile = path.resolve(filePath);
    if (fs.existsSync(pathToFile)) {
      fs.removeSync(pathToFile)
    }
    return true;
  }

  static removeFolder = async (folderPath: string): Promise<any> => {
    const pathToFolder = path.resolve(folderPath);
    if (fs.existsSync(pathToFolder)) {
      fs.rmdirSync(pathToFolder, { recursive: true })
    }
    return true
  }

  static getFileExtension = (fileName: string): string => {
    const length: number = fileName.split(".").length;
    const ext: string = fileName.split(".").pop();
    return length <= 1 ? "" : `.${ext}`;
  }

  static removeFromDisk = (pathFile: string): void => {
    pathFile = path.resolve(pathFile);
    if (fs.existsSync(pathFile)) {
      fs.removeSync(pathFile);
    }
  };

  static moveToDirectory = (oldPath: string, newDirectory: string):void => {
    const fileName = path.basename(oldPath);
    const pathFile = path.resolve(oldPath);
    const directory = path.resolve(newDirectory);
    const newPath = path.join(directory, fileName);
    if (fs.existsSync(pathFile)) {
      fs.ensureDirSync(directory);
      fs.renameSync(pathFile, newPath);
    }
  };

  static unzipFile = async (source: string, directory: string): Promise<void> => {
    return await extract(source, {dir: directory})
  }
}

export default FileUtil;
