import Smtp from './drivers/smtp';
import SendGrid from './drivers/sendGrid';
import MailBuilder from './builder';
import ApiException from '@root/app/Exceptions/ApiException';

interface MailerOption {
  from: string
  to: string
  subject: string
  text: string
  html: string
}

class Mailer {
  static async send(email, template, campaign) {
    let sender: Smtp | SendGrid;

    if (email.smtp) {
      sender = new Smtp(email.email, email.password);
    }
    else if (email.sendGrid) {
      sender = null;
    }
    else {
      sender = null;
    }

    if (!sender) throw new ApiException(400, "no mail driver");


    const html = await MailBuilder.buildHtml(template, campaign)


    const options: MailerOption = {
      from: 'Admin Test',
      to: 'hoang.the.bao@mqsolutions.com.vn',
      subject: 'helloooooooooooooo from the other side',
      text: 'helloooooooooooooo from the other side ',
      html: html,
    }

    return options;

    return await sender.sendMail(options)
  }
}

export default Mailer
