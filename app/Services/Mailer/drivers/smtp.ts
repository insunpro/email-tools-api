import Nodemailer, { Transporter } from "nodemailer";

class Smtp {
  transporter: Transporter;

  constructor(email: string, password: string) {
    this.transporter = Nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 587,
      secure: false,
      auth: {
        user: email,
        pass: password
      }
    });
  }

  async sendMail(options) {
    return await this.transporter.sendMail(options)
  }
}

export default Smtp;
