import sgMail from '@sendgrid/mail';

class SendGrid {
  transporter: typeof sgMail
  constructor (apiKey: string) {
    this.transporter = sgMail;
    this.transporter.setApiKey(apiKey);
  }

  async sendMail(options) {
    this.transporter.send(options)
  }
}

export default SendGrid
