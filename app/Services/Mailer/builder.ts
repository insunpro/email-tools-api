import fs from 'fs';
import path from 'path';
import Handlebars from 'handlebars';
import _ from 'lodash'
import env from '@core/Env'

const TEMPLATE_FOLDER = env.get('TEMPLATE_FOLDER');
class MailBuilder {
  static async buildHtml(template:string , data): Promise<any> {
    const templatePath = path.resolve(`${TEMPLATE_FOLDER}/default/index.hbs`)

    const templateData = fs.readFileSync(templatePath, 'utf8');

    const compiler = Handlebars.compile(templateData);
    return compiler(data)
  }

  static async buildSubject(template: string, data): Promise<any> {
    const templatePath = path.join(__dirname, 'templates', template, 'index.hbs')
    const templateData = await fs.readFileSync(templatePath, 'utf8');

    const compiler = Handlebars.compile(templateData);
    return compiler(data)
  }
}

export default MailBuilder
