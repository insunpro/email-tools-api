import BaseModel from './BaseModel'
import PermissionModel from './PermissionModel'
import RolePermissionModel from './RolePermissionModel'

export default class RoleModel extends BaseModel {
  static tableName = "roles"

  //fields
  id!: number;
  name!: string;
  description: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;
  permissions?: any[];
  role_permissions?: any[];
  numberOfUser: number;

  static get allowSelect(): string[] {
    return [
      'roles.id',
      'roles.name',
      'roles.createdAt',
      'roles.numberOfUser'
    ]
  }

  static get relationMappings(): any {
    return {
      role_permissions: {
        relation: BaseModel.HasManyRelation,
        modelClass: RolePermissionModel,
        join: {
          from: 'roles.id',
          to: 'role_permissions.roleId'
        }
      },
      permissions: {
        relation: BaseModel.ManyToManyRelation,
        modelClass: PermissionModel,
        join: {
          from: 'roles.id',
          through: {
            from: 'role_permissions.roleId',
            to: 'role_permissions.permissionId',
          },
          to: 'permissions.id',
        },
      }
    }
  }

  static async getPermissions(id: number): Promise<any> {
    const role = await this.query()
      .withGraphFetched('[role_permissions, permissions]')
      .findById(id)
    const result = {}
    if (!role) return result;
    for (const key in role.permissions) {
      result[role.permissions[key].keyword] = role.role_permissions[key].value
    }
    return result
  }

  async getPermission(permissionId: number): Promise<any> {
    return await this.$relatedQuery('permissions')
      .select('role_permissions.*')
      .where('role_permissions.permissionId', permissionId)
      .first()
  }

  async removePermission(permissionId: number): Promise<any> {
    return await this.$relatedQuery('permissions')
      .where('role_permissions.permissionId', permissionId)
      .del()
  }

  async updatePermission(permissionId: number, value: number): Promise<any> {
    return await this.$relatedQuery('permissions')
      .where('role_permissions.permissionId', permissionId)
      .update({ value: value })
  }

  async addPermission(permissionId: number, value: number, createdBy?: number): Promise<any> {
    return await this.$relatedQuery('permissions')
      .where('role_permissions.permissionId', permissionId)
      .insert({ permissionId: permissionId, value: value, createdBy: createdBy })
  }
}
