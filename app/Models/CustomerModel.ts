import BaseModel from "./BaseModel";

export default class CustomerModel extends BaseModel {
  static tableName = "customers";

  //fields
  id!: number;
  email: string;
  firstName: string;
  lastName: string;
  address: string;
  phone: string;
  createdAt: Date;
  updatedAt: Date;

  static get allowSelect(): string[] {
    return [
      "customers.id",
      "customers.email",
      "customers.firstName",
      "customers.lastName",
      "customers.address",
      "customers.phone",
      "customers.createdAt",
      "customers.updatedAt",
    ];
  }
}

