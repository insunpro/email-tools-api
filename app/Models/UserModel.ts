import BaseModel from "./BaseModel";

import ApiException from "@root/app/Exceptions/ApiException";
import bcrypt from "bcrypt";
import authConfig from "@config/auth";

export default class UserModel extends BaseModel {
  static tableName = "users";

  //fields
  id!: number;
  username!: string;
  password!: string;
  email: string;
  role: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;

  static get allowSelect(): string[] {
    return [
      "users.id",
      "users.username",
      "users.email",
      "users.role",
      "users.status",
      "users.createdAt",
      "users.updatedAt",
    ];
  }

  static async checkLogin({ username, password }: {username: string, password: string}): Promise<any> {
    const user = await this.query()
      // .select([...this.allowSelect, 'users.password'])
      .findOne({ username: username });
    if (!user) return false;
    // return user;
    const checkPassword = await this.compare(password, user.password);
    delete user.password;
    if (checkPassword) return user;
    return false;
  }

  static async hash(plainPassword: string): Promise<string> {
    return await bcrypt.hash(plainPassword + authConfig.SECRET_KEY, 10);
  }

  static async compare(
    plainPassword: string,
    encryptedPassword: string
  ): Promise<boolean> {
    return await bcrypt.compare(
      plainPassword + authConfig.SECRET_KEY,
      encryptedPassword
    );
  }

  async changePassword(newPassword: string): Promise<any> {
    newPassword = await UserModel.hash(newPassword);
    return await this.$query().patchAndFetchById(this.id, {
      password: newPassword,
    });
  }
}
