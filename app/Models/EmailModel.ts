import BaseModel from "./BaseModel";

export default class EmailModel extends BaseModel {
  static tableName = "emails";

  //fields
  id!: number;
  email: string;
  type: 'smtp' | 'sendGrid' | 'ses';
  information: any;
  note: string;
  createdAt: Date;
  updatedAt: Date;

  static get allowSelect(): string[] {
    return [
      "emails.id",
      "emails.email",
      "emails.type",
      "emails.information",
      "emails.note",
      "emails.createdAt",
      "emails.updatedAt",
    ];
  }

}

