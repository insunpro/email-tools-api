import BaseModel from './BaseModel'
import CampaignJobModel from './CampaignJobModel';
import EmailModel from './EmailModel';
import TemplateModel from './TemplateModel';

export default class CampaignModel extends BaseModel {
  static tableName = "campaigns"

  //fields
  id!: number;
  name!: string;
  templateId!: number;
  emailId: number;
  status: number;
  startTime: Date;
  endTime: Date;
  delayTime: number;
  createdAt: Date;
  updatedAt: Date;

  static get allowSelect(): string[] {
    return [
      'campaigns.id',
      'campaigns.name',
      'campaigns.templateId',
      'campaigns.emailId',
      'campaigns.status',
      'campaigns.startTime',
      'campaigns.endTime',
      'campaigns.delayTime',
      'campaigns.createdAt',
      'campaigns.updatedAt',
    ]
  }

  static get relationMappings(): any {
    return {
      jobs: {
        relation: BaseModel.HasManyRelation,
        modelClass: CampaignJobModel,
        join: {
          from: 'campaigns.id',
          to: 'campaign_jobs.campaignId'
        }
      },
      template: {
        relation: BaseModel.HasOneRelation,
        modelClass: TemplateModel,
        join: {
          from: 'campaigns.templateId',
          to: 'templates.id'
        }
      },
      email: {
        relation: BaseModel.HasOneRelation,
        modelClass: EmailModel,
        join: {
          from: 'campaigns.emailId',
          to: 'emails.id'
        }
      }
    }
  }
}

