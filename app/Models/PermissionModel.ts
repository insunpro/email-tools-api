import BaseModel from './BaseModel'

export default class PermissionModel extends BaseModel {
  static tableName = "permissions"

  //fields
  id: number;
  name: string;
  description: string;
  keyword: string;
  allowValue: number;
  createdAt: Date;
  updatedAt: Date;

  static async getByKeyword(keyword: string): Promise<any> {
    return await this.query().findOne({ keyword })
  }
}