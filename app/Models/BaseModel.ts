import Base from "@root/libs/core/Databases/BaseModel";
import ExtendQueryBuilder from "./QueryBuilder";

export default class BaseModel extends Base {
  //static QueryBuilder: typeof ExtendQueryBuilder = ExtendQueryBuilder
  QueryBuilderType!: ExtendQueryBuilder<this>;
  static QueryBuilder = ExtendQueryBuilder;
}
