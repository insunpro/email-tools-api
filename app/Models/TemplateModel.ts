import BaseModel from './BaseModel'

export default class TemplateModel extends BaseModel {
  static tableName = "templates"

  //fields
  id: number;
  name: number;
  thumbnail: string;
  sourcePath: string;
  filePath: string;
  fileName: string;
  fileSize: number;
  md5: string;
  createdAt: Date;
  updatedAt: Date;

  static get allowSelect(): string[] {
    return [
      'templates.id',
      'templates.name',
      'templates.thumbnail',
      'templates.sourcePath',
      'templates.filePath',
      'templates.fileName',
      'templates.fileSize',
      'templates.md5',
      'templates.createdAt',
      'templates.updatedAt',
    ]
  }

}
