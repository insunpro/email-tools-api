import BaseModel from "./BaseModel";
import UserModel from "./UserModel";

export default class UserLogModel extends BaseModel {
  static tableName = "user_logs";

  //fields
  uuid!: string;
  userId?: number;
  controller: string;
  action: string;
  data: any;
  before: any;
  isError: any;
  createdAt: Date;

  static get allowSelect(): string[] {
    return [
      "user_logs.userId",
      "user_logs.controller",
      "user_logs.action",
      "user_logs.data",
      "user_logs.before",
      "user_logs.isError",
      "user_logs.createdAt",
    ];
  }

  static get relationMappings(): any {
    return {
      user: {
        relation: BaseModel.HasOneRelation,
        modelClass: UserModel,
        join: {
          from: "user_logs.userId",
          to: "users.id",
        },
      },
    };
  }
}
