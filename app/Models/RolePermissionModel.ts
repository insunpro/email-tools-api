import BaseModel from "./BaseModel";
import PermissionModel from "./PermissionModel";
import RoleModel from "./RoleModel";

export default class RolePermissionModel extends BaseModel {
  static tableName = "role_permissions";

  //fields
  id: number;
  roleId: number;
  permissionId: number;
  keyword: string;
  value: number;
  createdAt: Date;
  createdBy: number;
  role?: any;
  permission?: any;

  static get relationMappings(): any {
    return {
      permission: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: PermissionModel,
        join: {
          from: "role_permissions.permissionId",
          to: "permissions.id",
        },
      },
      role: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: RoleModel,
        join: {
          from: "role_permissions.roleId",
          to: "roles.id",
        },
      },
    };
  }
}
