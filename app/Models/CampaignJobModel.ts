import BaseModel from './BaseModel'
import CampaignModel from './CampaignModel';
import CustomerModel from './CustomerModel';

export default class CampaignJobModel extends BaseModel {
  static tableName = "campaign_jobs"

  //fields
  id!: string;
  campaignId: number;
  customerId: number;
  startTime: Date;
  endTime: Date;
  log: string;
  status: number;
  createdAt: Date;
  updatedAt: Date;

  static get allowSelect(): string[] {
    return [
      'campaign_jobs.id',
      'campaign_jobs.campaignId',
      'campaign_jobs.customerId',
      'campaign_jobs.startTime',
      'campaign_jobs.endTime',
      'campaign_jobs.log',
      'campaign_jobs.status',
      'campaign_jobs.createdAt',
      'campaign_jobs.updatedAt'
    ]
  }

  static get relationMappings(): any {
    return {
      campaign: {
        relation: BaseModel.BelongsToOneRelation,
        modelClass: CampaignModel,
        join: {
          from: 'campaign_jobs.campaignId',
          to: 'campaigns.id'
        }
      },
      customer: {
        relation: BaseModel.HasOneRelation,
        modelClass: CustomerModel,
        join: {
          from: 'campaign_jobs.customerId',
          to: 'customers.id'
        }
      },
    }
  }
}