import BaseSchedule from './BaseSchedule'
import moment from 'moment'
import CampaignController from '@app/Controllers/CampaignController'
export default class CampaignSchedule extends BaseSchedule {
  static schedule = '0 * * * * *'

  static async handle() {
    console.log("[================ CampaignSchedule ================]")

  }

  static async getCampaigns() {
    const campaignController = new CampaignController()
    return await campaignController.getCampaigns()
  }

  static async getJobs(campaignId) {
    const campaignController = new CampaignController()
    return await campaignController.getJobs(campaignId)
  }

  static async createJob() {
    return true
  }
}
