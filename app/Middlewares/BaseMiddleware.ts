import { Response, Request, NextFunction } from "express";

 interface ExtendResponse extends Response {
  requestId: string;
  success: () => void;
  error: () => void;
  sent: number;
}
interface ExtendRequest extends Request {
  all: () => any;
  getHeaders: () => any;
  authUser: any;
  auth: any
}

export default class BaseMiddleware {
  request: ExtendRequest;
  response: ExtendResponse;
  next: NextFunction;

  constructor(request: ExtendRequest, response: ExtendResponse, next: NextFunction) {
    this.request = request;
    this.response = response;
    this.next = next;
  }
}
