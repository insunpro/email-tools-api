import Server from '@root/libs/core/Server'
import Cronjobs from '@app/Cronjobs'
(async () => {
  try {
    const server = new Server();
    await server.start()
    Cronjobs.start()
  } catch (e) {
    console.error(e);
    process.exit(1);
  }
})();
