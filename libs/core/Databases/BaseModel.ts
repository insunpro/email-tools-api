import { Model } from 'objection';
import ConnectDB from '@root/libs/core/Databases'

Model.knex(ConnectDB());

export default Model
