import dbConfig from "@config/database";
import knex from "knex";

const options = {
  client: dbConfig.DB_TYPE,
  connection: {
    host: dbConfig.DB_HOST,
    port: dbConfig.DB_PORT,
    user: dbConfig.DB_USER,
    password: dbConfig.DB_PASS,
    database: dbConfig.DB_NAME,
    multipleStatements: true,
    timezone: "+00:00",
  },
  pool: { min: 0, max: dbConfig.DB_POOL_SIZE },
};

export default class Connection {
  private _connection: any;

  get connection(): any {
    if (this._connection && !this._connection.connecting) {
      this.connect();
    }
    return this._connection;
  }

  set connection(connection) {
    this._connection = connection;
    this._connection.connecting = true;
  }

  connect(): any {
    console.log("new Database connection");
    try {
      if (typeof knex.QueryBuilder.extend !== "function") {
        throw Error(
          "Knex version đã cũ, xóa node_modules, yarn.lock và cài lại node modules"
        );
      }
      this.connection = knex(options);
      return this.connection;
    } catch (e) {
      console.log("Connection.connect.error", e);
      throw e;
    }
  }
}
