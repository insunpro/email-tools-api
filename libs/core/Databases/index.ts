import Connection from "./Connection";

export default function connectDB(): any {
  const connection = new Connection();
  return connection.connect();
}
