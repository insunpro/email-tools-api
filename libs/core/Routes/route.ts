import Exception from "@root/libs/core/Exception";
import UrlPattern from "url-pattern";
import express from "express";
import RoutesIndex from "./index";
import * as Controllers from "@app/Controllers";

export default class Route {
  router: any;
  url: string;
  action: any;
  method: string;
  middlewares: any;
  _parent: string;
  _name: string;
  _sidebar: string;

  constructor(url: string, action: string, method: string) {
    this.router = express.Router();
    this.url = url;
    this.action = action;
    this.method = method;
    this.middlewares = [];
  }

  //Route.get().middleware([mid1, mid2....])
  middleware(middlewares): any {
    this.middlewares = [...this.middlewares, ...middlewares];
    return this;
  }

  /**
   * Convert ActionPath sang function
   * UserController.index ==> function index trong UserController.
   */
  _getActionFromPath(actionPath: string, request, response): any {
    const [controllerName, actionName] = actionPath.split(".");
    if (controllerName == undefined || actionName == undefined) {
      throw new Error(`Action does not exist: ${actionPath}`);
    }

    try {
      const controller = new Controllers[controllerName]();
      if (typeof controller[actionName] !== "function") {
        throw new Error(`Action does not exist: ${actionPath}`);
      }

      controller.request = request;
      controller.response = response;
      request.audit = {
        controller: controllerName,
        action: actionName,
      };
      return controller[actionName].bind(controller);
    } catch (e) {
      console.log(e);
      throw e;
    }
  }

  /**
   * Hàm tạo ra các router
   */
  build(prefixName: string, prefixRoute: string, middlewares = []): any {
    if (middlewares.length > 0) {
      this.middlewares = [...middlewares, ...this.middlewares];
    }
    //register routename
    const parent = this._parent
      ? prefixName != null
        ? `${prefixName}.${this._parent}`
        : this._parent
      : undefined;
    let url = prefixRoute != null ? `${prefixRoute}/${this.url}` : this.url;
    url = url.replace(/\/+/g, "/").replace(/\/+$/, "");
    if (!url) url = "/";
    let route: any = {
      url: url,
      parent: parent,
      action: this.action + "",
      method: this.method,
      middlewares: this.middlewares,
    };

    if (this._name != null) {
      const Group = RoutesIndex;
      const name =
        prefixName != null ? `${prefixName}.${this._name}` : this._name;
      const sidebar = this._sidebar
        ? prefixName != null
          ? `${prefixName}.${this._sidebar}`
          : this._sidebar
        : name;

      route = {
        ...route,
        name: name,
        sidebar: sidebar,
      };
      Group.routes = {
        ...Group.routes,
        [name]: route,
      };
    }

    this.router[this.method](
      this.url,
      (request, response, next) => {
        request.route = route;
        next();
      },
      ...this.middlewares,
      async (request, response, next) => {
        try {
          let action = this.action;
          if (typeof action === "function") {
            const result = await action({
              request,
              response,
              next,
            });
            if (result) {
              response.send(result);
            }
          } else {
            action = this._getActionFromPath(action, request, response);
            const result = await action({
              request,
              response,
              next,
            });
            if (result !== false) response.success(result);
            if (!response.headersSent && result !== false) {
              response.end();
            }
          }
        } catch (e) {
          Exception.handle(e, request, response);
        }
      }
    );
    return this.router;
  }

  name(routeName: string): Route {
    this._name = routeName;
    return this;
  }
  sidebar(sidebarSelected: string): Route {
    this._sidebar = sidebarSelected;
    return this;
  }
  parent(name: string): Route {
    this._parent = name;
    return this;
  }

  makeUrl(params): string {
    const pattern = new UrlPattern(this.url);
    return pattern.stringify(params);
  }
}
