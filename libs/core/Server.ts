import dotenv from "dotenv";
dotenv.config();

import express, { Response } from "express";
import AppRoutes from "./Routes";
import ConnectDB from "@root/libs/core/Databases";
import bodyParser from "body-parser";
import cors from "cors";


export default class Server {
  express = express();
  host: string;
  port: number;
  options: any;

  constructor({
    host,
    port,
    options = {},
  }: { host?: string; port?: string | number; options?: any } = {}) {
    const defaultPORT = process.env.PORT;
    this.host = host || process.env.HOST || "0.0.0.0";
    this.port = Number(port) || Number(defaultPORT) || 3333;
    this.options = options;
  }

  async start(): Promise<any> {
    ConnectDB();

    this.express.use(bodyParser.urlencoded({ extended: true, limit: "500mb" }));
    this.express.use(
      bodyParser.json({
        limit: "500mb",
      })
    );

    this.express.use(cors());
    // this.express.options('*', cors(corsConfig))

    this.express.use(AppRoutes.build());

    this.express.all("*", (req, res, next) => {
      // Handle request
      res.header("Access-Control-Allow-Origin", "*");
      // res.header('Access-Control-Allow-Credentials', 'true');
      res.header(
        "Access-Control-Allow-Headers",
        "Content-Type,accept,access_token,X-Requested-With"
      );
      res.sendStatus(404);
      next();
    });

    await new Promise<void>((r) =>
      this.express.listen(this.port, this.host, () => {
        console.log(`server stated: ${this.host}:${this.port}`);
        r();
      })
    );
    return {
      express: this.express,
    };
  }
}
