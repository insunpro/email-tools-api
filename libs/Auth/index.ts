import jwt from "jsonwebtoken";
import authConfig from '@config/auth'

const generateJWT = (data, options = {}): any => {
  const { key, ...otherOptions }: any = options
  return jwt.sign(data, key || authConfig.SECRET_KEY, otherOptions)
}
const decodeJWT = async (token: string, options = {}): Promise<any> => {
  let { key }: any = options
  key = key || authConfig.SECRET_KEY
  return jwt.verify(token, key);
}
const verify = async (token: string, options = {}): Promise<any> => {
  let { key }: any = options
  key = key || authConfig.SECRET_KEY
  return jwt.verify(token, key);
}

export default {
  generateJWT,
  decodeJWT,
  verify
}