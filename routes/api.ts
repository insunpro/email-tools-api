import AuthApiMiddlewareFn from '@root/app/Middlewares/AuthApiMiddleware';
import ExtendMiddlewareFn from '@root/app/Middlewares/ExtendMiddleware';
import AppRoutes from '@root/libs/core/Routes'
import multer from 'multer';

const upload = multer();

export default function BuildApi(): void {
  AppRoutes.createGroup(() => {
    /**************************************** Auth Routes (NOT REQUIRE LOGIN) ****************************************/
    AppRoutes.post("/login", "AuthController.login").name('auth.login')
    AppRoutes.post("/forgotPassword", "AuthController.forgotPassword").name('auth.forgotPassword')
    AppRoutes.post("/checkToken", "AuthController.checkToken").name('auth.checkToken')
    AppRoutes.post("/resetPassword", "AuthController.resetPassword").name('auth.resetPassword')
    /**************************************** End ****************************************/
  
    /**************************************** Test (NOT REQUIRE LOGIN) ****************************************/
    AppRoutes.post("/test/mail", "TestController.testMail").name("test.testMail")
    /**************************************** END ****************************************/
  
    /**************************************** REQUIRE LOGIN ****************************************/
    AppRoutes.createGroup(() => {
      //---------------------------------- User Api ---------------------------------------//
      AppRoutes.resource("/users", "UserController").name('users')
      AppRoutes.post("/users/:id/change-password", "UserController.changePassword").name("users.changePassword")
      //---------------------------------- End ---------------------------------------//
  
      //---------------------------------- Email api ---------------------------------------//
      AppRoutes.resource("/emails", "EmailController").name('emails')
      //---------------------------------- End ---------------------------------------//
  
      //---------------------------------- Customer Api ---------------------------------------//
      AppRoutes.resource("/customers", "CustomerController").name('customers')
      AppRoutes.post("/customers/import", "CustomerController.storeMany").name('customers.import')
      //---------------------------------- End ---------------------------------------//
  
      //---------------------------------- Campaign Api ---------------------------------------//
      AppRoutes.resource("/campaigns", "CampaignController").name('campaigns')
      //---------------------------------- End ---------------------------------------//
  
      //---------------------------------- Template Api ---------------------------------------//
      AppRoutes.resource("/templates", "TemplateController").name('templates').middleware([upload.any()])
      //---------------------------------- End ---------------------------------------//
    }).middleware([AuthApiMiddlewareFn])
    /**************************************** END REQUIRE LOGIN ****************************************/
  }).middleware([ExtendMiddlewareFn]).name('api').prefix('/api')
}

