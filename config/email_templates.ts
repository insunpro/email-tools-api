export default {
  money: {
    subject: "Mail sent out of money warning",
    content: "Mail sent out of money warning"
  },
  login: {
    subject: "You need to login again for the bot",
    content: `<html>
      <a href=${process.env.DOMAIN}/staff/login?id={{id}}&hash={{hash}}>Click here!</a>
      The system will proceed to automatically log in to DNC with the corpPass account. It needs to be verified on the owner's SingPass Mobile. So contact the account owner and make sure they're ready to confirm.
    </html>`
  },
  forgotPassword: {
    subject: "Reset of account password",
    content: `<h3>Dear {{firstName}}. {{lastName}}</h3><p>You have requested a new password for your account. Please reset your password by clicking on the link below:</p><p><a href="{{resetPasswordLink}}" target="_blank">Click here</a></p>`
  }
}
