
exports.seed = function (knex, Promise) {
  const data = [
    {
      "username": "admin",
      "password": "$2b$10$8p0N8y.VH2H30DkwSWX0QeHvi1JYF4NY5J4G1mEdK.3ZLUfOx.STi",
      "role": "admin"
    },
    {
      "username": "user",
      "password": "$2b$10$iFSOtpDfu2T/g5p9PlvkR.fFS08Sejn79hfzK0GCRSPijAFwE4R9W",
      "role": "user"
    }
  ]

  // Deletes ALL existing entries
  return knex('users').del()
    .then(async () => {
      await knex('users').insert(data);
      await knex.raw('select setval(\'users_id_seq\', max(id)) from users');
    });
};
