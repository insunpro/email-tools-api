
class Triggers {
    constructor(tableName) {
        this.triggers = ['insert', 'update', 'delete'];
        this.states = ['before', 'after']
        this.table = tableName
    }

    /**
     * @param {*} state ['before', 'after'] -- as Timing in MySql
     * @param {*} trigger ['insert', 'update', 'delete'] -- as Event in MySql
     * @param {*} rawQuery SQL raw query -- as Statement in MySql
     */
    create = (state, trigger, rawQuery) => {
        state = this.validateState(state);
        trigger = this.validateTrigger(trigger);
        rawQuery = this.validateQuery(rawQuery);
        return `
        CREATE TRIGGER ${state}_${this.table}_${trigger}
        ${state.toUpperCase()} ${trigger.toUpperCase()} 
        ON ${this.table} FOR EACH ROW
        BEGIN
            ${rawQuery}
        END
      `
    }

    /**
     * @param {*} state ['before', 'after'] -- as Timing in MySql
     * @param {*} trigger ['insert', 'update', 'delete'] -- as Event in MySql
     */
    remove = (state, trigger) => {
        this.validateState(state);
        this.validateTrigger(trigger);
        return `
        DROP TRIGGER IF EXISTS ${state}_${this.table}_${trigger}
        `
    }

    /** validate and format data here */
    validateTrigger = (trigger) => {
        if (!this.triggers.includes(trigger)) throw new Error('trigger not allow');
        return trigger;
    }

    validateState = (state) => {
        if (!this.states.includes(state)) throw new Error('state not allow');
        return state;
    }

    validateQuery = (rawQuery) => {
        rawQuery = rawQuery.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "");
        if (rawQuery.slice(rawQuery.length - 1) != ';') rawQuery += ';'
        return rawQuery;
    }
}

module.exports = Triggers