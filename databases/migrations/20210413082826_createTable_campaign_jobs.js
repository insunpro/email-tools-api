
exports.up = function (knex) {
  return knex.schema.createTable('campaign_jobs', function (table) {
      table.uuid('id').notNullable().primary().defaultTo(knex.raw('uuid_generate_v4()'));
      table.integer('campaignId').notNullable().index().references('id').inTable('campaigns')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table.integer('customerId').notNullable().index().references('id').inTable('customers')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table.timestamp('startTime').nullable();
      table.timestamp('endTime').nullable();
      table.text('log').nullable();
      table.integer('status').defaultTo(0);
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
  })
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('campaign_jobs');
};
