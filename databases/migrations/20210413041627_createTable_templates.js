
exports.up = function (knex) {
  return knex.schema.createTable('templates', function (table) {
      table.increments('id');
      table.integer('userId').notNullable().references('id').inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table.string('name').notNullable();
      table.string('thumbnail').nullable();
      table.string('sourcePath').nullable();
      table.string('fileName').nullable();
      table.string("filePath").nullable();
      table.decimal('fileSize', 16, 0).nullable();
      table.string('md5').nullable();
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
  })
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('templates');
};
