
exports.up = function (knex) {
  return knex.schema.createTable('emails', function (table) {
      table.increments('id');
      table.integer('userId').notNullable().references('id').inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table.string('email').notNullable();
      table.string('type').notNullable();
      table.jsonb('information').nullable();
      table.text('note').nullable();
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
  })
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('emails');
};
