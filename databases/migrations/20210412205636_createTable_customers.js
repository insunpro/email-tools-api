
exports.up = function (knex) {
  return knex.schema.createTable('customers', function (table) {
      table.increments('id');
      table.integer('userId').notNullable().references('id').inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table.string('email').notNullable();
      table.string('firstName').nullable();
      table.string('lastName').nullable();
      table.string('address').nullable();
      table.string('phone').nullable();
      table.integer('status').defaultTo(0);
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
  })
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('customers');
};
