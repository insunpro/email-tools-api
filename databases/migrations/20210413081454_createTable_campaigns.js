
exports.up = function (knex) {
  return knex.schema.createTable('campaigns', function (table) {
      table.increments('id');
      table.integer('userId').notNullable().references('id').inTable('users')
        .onUpdate('CASCADE')
        .onDelete('CASCADE');
      table.string('name').notNullable();
      table.integer('emailId').nullable().index().references('id').inTable('emails')
        .onUpdate('CASCADE')
        .onDelete('SET NULL');
      table.integer('templateId').nullable().index().references('id').inTable('templates')
        .onUpdate('CASCADE')
        .onDelete('SET NULL');
      table.timestamp('startTime').nullable();
      table.timestamp('endTime').nullable();
      table.integer('delayTime').defaultTo(5).comment("Thời gian giữa các lần gửi mail");
      table.integer('status').defaultTo(0);
      table.timestamp('createdAt').defaultTo(knex.fn.now());
      table.timestamp('updatedAt').defaultTo(knex.fn.now());
  })
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('campaigns');
};
